<p align="center">
  <a href="//zmz-ui3.zhuimengzhu.com">
    <img width="100" src="//www.zhuimengzhu.com/static/common/image/zmz-ui.png">
  </a>
</p>

# 公告
``zmz-ui3``项目开源，供大家学习！如果有问题或者想开发一个定制组件请联系我们（加q群:581764678）

# ZMZ UI

`zmz-ui3` 是一款基于 `Vue.js 3.x` 的前端 UI 组件库，主要用于快速开发
[演示地址](//zmz-ui3.zhuimengzhu.com)

# Logs 更新日志
### [ v0.0.1 ] 2025-03-05
* vue2升级vue3

# 特性

- 基于 `Vue3` 开发的 UI 组件
- 使用 npm + webpack + babel 的工作流，支持 ES2015
- 提供友好的 API，可灵活的使用组件

# 浏览器支持
- 适用于 Vue.js 3，兼容现代浏览器，不支持 IE

# 安装

- yarn

```bash
yarn add zmz-ui3
```

- npm

```bash
npm install zmz-ui3 --save
```

# 使用

```js
import ZMZUI3 from 'zmz-ui3' // 引入组件库
import 'zmz-ui3/packages/theme-default/lib/index.css' // 引入样式库

app.use(ZMZUI3)
```

# 贡献

如果你在使用 `zmz-ui3` 时遇到问题，或者有好的建议，欢迎给我们提Issue
[github][Issue](https://github.com/jiawenguang/zmz-ui3)

[gitee][Issue](https://gitee.com/zmpig/zmz-ui3/issues)

# License

MIT