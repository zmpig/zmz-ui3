/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzLink from './src/zmz-link.vue';

ZmzLink.install = function(app) {
    app.component(ZmzLink.name, ZmzLink);
};

export default ZmzLink;
