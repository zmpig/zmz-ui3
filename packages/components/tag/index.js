/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzTag from './src/zmz-tag.vue'

ZmzTag.install = function(app) {
    app.component(ZmzTag.name, ZmzTag)
}

export default ZmzTag
