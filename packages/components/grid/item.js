/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */

import ZmzGridItem from './src/zmz-grid-item.vue';

ZmzGridItem.install = function(app) {
    app.component(ZmzGridItem.name, ZmzGridItem);
};

export default ZmzGridItem;
