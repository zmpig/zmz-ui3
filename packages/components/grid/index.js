/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */

import ZmzGrid from './src/zmz-grid.vue';

ZmzGrid.install = function(app) {
    app.component(ZmzGrid.name, ZmzGrid);
};

export default ZmzGrid;
