/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzScroll from './src/zmz-scroll.vue';

ZmzScroll.install = function(app) {
    app.component(ZmzScroll.name, ZmzScroll);
};

export default ZmzScroll;
