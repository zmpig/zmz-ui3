let ZmzCopy = (data) => {
    return new Promise((resolve) => {
        var inp = document.createElement('textarea');
        document.body.appendChild(inp);
        inp.value = data;
        inp.select();
        try {
            document.execCommand('copy', false);
            resolve(true)
        } catch(e) {
            resolve(false)
        }
        inp.remove();
    })
}

export default ZmzCopy
