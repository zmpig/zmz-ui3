/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzImagePreview from './src/zmz-image-preview.vue';

ZmzImagePreview.install = function(app) {
    app.component(ZmzImagePreview.name, ZmzImagePreview);
};

export default ZmzImagePreview;
