/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzImage from './src/zmz-image.vue';

ZmzImage.install = function(app) {
    app.component(ZmzImage.name, ZmzImage);
};

export default ZmzImage;
