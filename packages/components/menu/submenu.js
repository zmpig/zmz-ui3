/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzSubmenu from './src/zmz-menu-submenu.vue';

ZmzSubmenu.install = function(app) {
    app.component(ZmzSubmenu.name, ZmzSubmenu);
};

export default ZmzSubmenu;
