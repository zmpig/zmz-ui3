/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzMenuItemGroup from './src/zmz-menu-group.vue';

ZmzMenuItemGroup.install = function(app) {
    app.component(ZmzMenuItemGroup.name, ZmzMenuItemGroup);
};

export default ZmzMenuItemGroup;
