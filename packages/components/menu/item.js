/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzMenuItem from './src/zmz-menu-item.vue';

ZmzMenuItem.install = function(app) {
    app.component(ZmzMenuItem.name, ZmzMenuItem);
};

export default ZmzMenuItem;
