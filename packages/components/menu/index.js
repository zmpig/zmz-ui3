/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzMenu from './src/zmz-menu.vue';

ZmzMenu.install = function(app) {
    app.component(ZmzMenu.name, ZmzMenu);
};

export default ZmzMenu;
