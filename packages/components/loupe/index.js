/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzLoupe from './src/zmz-loupe.vue';

ZmzLoupe.install = function(app) {
    app.component(ZmzLoupe.name, ZmzLoupe);
};

export default ZmzLoupe;
