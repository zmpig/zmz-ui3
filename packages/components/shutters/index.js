/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzShutters from './src/zmz-shutters.vue';

ZmzShutters.install = function(app) {
    app.component(ZmzShutters.name, ZmzShutters);
};

export default ZmzShutters;
