/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzShuttersItem from './src/zmz-shutters-item.vue';

ZmzShuttersItem.install = function(app) {
    app.component(ZmzShuttersItem.name, ZmzShuttersItem);
};

export default ZmzShuttersItem;
