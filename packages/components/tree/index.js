/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzTree from './src/zmz-tree.vue';

ZmzTree.install = function(app) {
    app.component(ZmzTree.name, ZmzTree);
};

export default ZmzTree;
