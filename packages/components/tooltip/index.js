/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzTooltip from './src/zmz-tooltip.vue'

ZmzTooltip.install = function(app) {
    app.component(ZmzTooltip.name, ZmzTooltip)
}

export default ZmzTooltip
