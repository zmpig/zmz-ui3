/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzTabsItem from './src/zmz-tabs-item.vue';

ZmzTabsItem.install = function (app) {
  app.component(ZmzTabsItem.name, ZmzTabsItem);
};

export default ZmzTabsItem;
