/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzTabs from './src/zmz-tabs.js';

ZmzTabs.install = function (app) {
  app.component(ZmzTabs.name, ZmzTabs);
};

export default ZmzTabs;
