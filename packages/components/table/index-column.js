/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzTableColumn from './src/zmz-table-column.vue';

ZmzTableColumn.install = function (Vue) {
  Vue.component(ZmzTableColumn.name, ZmzTableColumn);
};

export default ZmzTableColumn;
