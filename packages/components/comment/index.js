/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzComment from './src/zmz-comment.vue';

ZmzComment.install = function(app) {
    app.component(ZmzComment.name, ZmzComment);
};

export default ZmzComment;
