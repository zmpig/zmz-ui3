/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzCommentItem from './src/zmz-comment-item.vue';

ZmzCommentItem.install = function(app) {
    app.component(ZmzCommentItem.name, ZmzCommentItem);
};

export default ZmzCommentItem;
