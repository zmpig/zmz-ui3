/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzDescriptionsItem from './src/zmz-descriptions-item.vue';

ZmzDescriptionsItem.install = function (app) {
    app.component(ZmzDescriptionsItem.name, ZmzDescriptionsItem);
};

export default ZmzDescriptionsItem;