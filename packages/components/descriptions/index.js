/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzDescriptions from './src/zmz-descriptions.vue';

ZmzDescriptions.install = function (app) {
    app.component(ZmzDescriptions.name, ZmzDescriptions);
};

export default ZmzDescriptions;