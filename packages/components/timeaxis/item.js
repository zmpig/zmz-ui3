/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzTimeaxisItem from './src/zmz-timeaxis-item.vue'

ZmzTimeaxisItem.install = function(app) {
    app.component(ZmzTimeaxisItem.name, ZmzTimeaxisItem)
}

export default ZmzTimeaxisItem
