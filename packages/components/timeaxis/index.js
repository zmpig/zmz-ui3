/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzTimeaxis from './src/zmz-timeaxis.vue'

ZmzTimeaxis.install = function(app) {
    app.component(ZmzTimeaxis.name, ZmzTimeaxis)
}

export default ZmzTimeaxis
