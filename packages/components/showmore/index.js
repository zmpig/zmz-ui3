/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzShowMore from './src/zmz-showmore.vue'

ZmzShowMore.install = function(app) {
    app.component(ZmzShowMore.name, ZmzShowMore)
}

export default ZmzShowMore
