/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzDropdown from './src/zmz-dropdown.vue';

ZmzDropdown.install = function(app) {
    app.component(ZmzDropdown.name, ZmzDropdown);
};

export default ZmzDropdown;
