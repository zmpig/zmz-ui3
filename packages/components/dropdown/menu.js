/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzDropdownMenu from './src/zmz-dropdown-menu.vue';

ZmzDropdownMenu.install = function(app) {
    app.component(ZmzDropdownMenu.name, ZmzDropdownMenu);
};

export default ZmzDropdownMenu;
