/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzDropdownItem from './src/zmz-dropdown-item.vue';

ZmzDropdownItem.install = function(app) {
    app.component(ZmzDropdownItem.name, ZmzDropdownItem);
};

export default ZmzDropdownItem;
