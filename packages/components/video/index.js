/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzVideo from './src/zmz-video.vue'

ZmzVideo.install = function(app) {
    app.component(ZmzVideo.name, ZmzVideo)
}

export default ZmzVideo
