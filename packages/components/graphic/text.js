/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzGraphicText from './src/zmz-graphic-text.vue';

ZmzGraphicText.install = function(app) {
    app.component(ZmzGraphicText.name, ZmzGraphicText);
};

export default ZmzGraphicText;
