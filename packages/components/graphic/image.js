/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzGraphicImage from './src/zmz-graphic-image.vue';

ZmzGraphicImage.install = function(app) {
    app.component(ZmzGraphicImage.name, ZmzGraphicImage);
};

export default ZmzGraphicImage;
