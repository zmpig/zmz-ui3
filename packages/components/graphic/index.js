/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzGraphic from './src/zmz-graphic.vue';

ZmzGraphic.install = function(app) {
    app.component(ZmzGraphic.name, ZmzGraphic);
};

export default ZmzGraphic;
