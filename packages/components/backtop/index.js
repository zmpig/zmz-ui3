/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzBacktop from './src/zmz-backtop.vue'

ZmzBacktop.install = function(app) {
    app.component(ZmzBacktop.name, ZmzBacktop)
}

export default ZmzBacktop
