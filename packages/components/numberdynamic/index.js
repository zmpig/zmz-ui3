/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzNumberdynamic from './src/zmz-numberdynamic.vue'

ZmzNumberdynamic.install = function(app) {
    app.component(ZmzNumberdynamic.name, ZmzNumberdynamic)
}

export default ZmzNumberdynamic
