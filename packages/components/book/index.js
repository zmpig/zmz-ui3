/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzBook from './src/zmz-book.vue';

ZmzBook.install = function(app) {
    app.component(ZmzBook.name, ZmzBook);
};

export default ZmzBook;
