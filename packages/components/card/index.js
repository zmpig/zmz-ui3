/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzCard from './src/zmz-card.vue';

ZmzCard.install = function (app) {
  app.component(ZmzCard.name, ZmzCard);
};

export default ZmzCard;
