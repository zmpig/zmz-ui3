/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */

import ZmzContainer from './src/zmz-container.vue';

ZmzContainer.install = function (app) {
  app.component(ZmzContainer.name, ZmzContainer);
};

export default ZmzContainer;
