/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzDivider from './src/zmz-divider.vue';

ZmzDivider.install = function(app) {
    app.component(ZmzDivider.name, ZmzDivider);
};

export default ZmzDivider;
