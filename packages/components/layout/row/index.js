/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */

import ZmzRow from './src/zmz-row.js';

ZmzRow.install = function(app) {
    app.component(ZmzRow.name, ZmzRow);
};

export default ZmzRow;
