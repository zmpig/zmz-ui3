/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */

import ZmzLayout from './src/zmz-layout.js';

ZmzLayout.install = function(app) {
    app.component(ZmzLayout.name, ZmzLayout);
};

export default ZmzLayout;
