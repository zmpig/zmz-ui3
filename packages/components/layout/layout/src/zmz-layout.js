import {
    h,
    computed,
    provide,
    ref,
    watch
} from 'vue'
export default {
    name: 'ZmzLayout',
    componentName: 'ZmzLayout',
    props: {
        /**
         * 间距
         */
        gutter: Number,
        /**
         * 生成标签
         */
        tag: {
            type: String,
            default: 'div'
        },
    },
    setup(props, {
        slots
    }) {
        const gutterRef = ref(props.gutter)
        const zmzLayoutStyle = computed(() => {
            let params = {}
            if (gutterRef.value) {
                params['paddingLeft'] = gutterRef.value + 'px';
                params['paddingRight'] = params['paddingLeft'];
            }
            return params
        })

        watch(() => props.gutter, (val) => {
            gutterRef.value = val
        })

        provide('layoutGutter', gutterRef)

        return () => h(props.tag, {
            class: ['zmz-layout'],
            style: zmzLayoutStyle.value
        }, slots.default())
    }
};
