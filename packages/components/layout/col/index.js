/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */

import ZmzCol from './src/zmz-col.js';

ZmzCol.install = function(app) {
    app.component(ZmzCol.name, ZmzCol);
};

export default ZmzCol;
