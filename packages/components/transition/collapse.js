/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzTransitionCollapse from './src/zmz-transition-collapse.js';

ZmzTransitionCollapse.install = function(app) {
    app.component(ZmzTransitionCollapse.name, ZmzTransitionCollapse);
};

export default ZmzTransitionCollapse;
