/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzAlbum from './src/zmz-album.vue';

ZmzAlbum.install = function(app) {
    app.component(ZmzAlbum.name, ZmzAlbum);
};

export default ZmzAlbum;
