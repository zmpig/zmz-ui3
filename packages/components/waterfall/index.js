/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzWaterfall from './src/zmz-waterfall.vue';

ZmzWaterfall.install = function(app) {
    app.component(ZmzWaterfall.name, ZmzWaterfall);
};

export default ZmzWaterfall;
