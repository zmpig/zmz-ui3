/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzWaterfallItem from './src/zmz-waterfall-item.vue';

ZmzWaterfallItem.install = function(app) {
    app.component(ZmzWaterfallItem.name, ZmzWaterfallItem);
};

export default ZmzWaterfallItem;
