/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzBadge from './src/zmz-badge.vue';

ZmzBadge.install = function(app) {
    app.component(ZmzBadge.name, ZmzBadge);
};

export default ZmzBadge;
