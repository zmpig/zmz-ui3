/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzPanel from './src/zmz-panel.vue';

ZmzPanel.install = function(app) {
    app.component(ZmzPanel.name, ZmzPanel);
};

export default ZmzPanel;
