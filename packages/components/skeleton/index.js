/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzSkeleton from './src/zmz-skeleton.vue';

ZmzSkeleton.install = function(app) {
    app.component(ZmzSkeleton.name, ZmzSkeleton);
};

export default ZmzSkeleton;
