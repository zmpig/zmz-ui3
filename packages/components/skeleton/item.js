/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzSkeletonItem from './src/zmz-skeleton-item.vue';

ZmzSkeletonItem.install = function(app) {
    app.component(ZmzSkeletonItem.name, ZmzSkeletonItem);
};

export default ZmzSkeletonItem;
