/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzAmount from './src/zmz-amount.vue';

ZmzAmount.install = function(app) {
    app.component(ZmzAmount.name, ZmzAmount);
};

export default ZmzAmount;
