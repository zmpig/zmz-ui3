/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzEmpty from './src/zmz-empty.vue';

ZmzEmpty.install = function (app) {
    app.component(ZmzEmpty.name, ZmzEmpty);
};

export default ZmzEmpty;
