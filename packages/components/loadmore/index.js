/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzLoadmore from './src/zmz-loadmore.vue';

ZmzLoadmore.install = function(app) {
    app.component(ZmzLoadmore.name, ZmzLoadmore);
};

export default ZmzLoadmore;
