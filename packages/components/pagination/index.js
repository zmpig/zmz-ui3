/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzPagination from './src/zmz-pagination.js';

ZmzPagination.install = function(app) {
    app.component(ZmzPagination.name, ZmzPagination);
};

export default ZmzPagination;
