/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzAvatar from './src/zmz-avatar.vue';

ZmzAvatar.install = function(app) {
    app.component(ZmzAvatar.name, ZmzAvatar);
};

export default ZmzAvatar;
