/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzAvatarGroup from './src/zmz-avatar-group.vue';

ZmzAvatarGroup.install = function (app) {
    app.component(ZmzAvatarGroup.name, ZmzAvatarGroup);
};

export default ZmzAvatarGroup;
