/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */

import ZmzButon from './src/zmz-button.js';

ZmzButon.install = function(app) {
    app.component(ZmzButon.name, ZmzButon);
};

export default ZmzButon;
