/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzButtonGroup from './src/zmz-button-group.vue';

ZmzButtonGroup.install = function(app) {
    app.component(ZmzButtonGroup.name, ZmzButtonGroup);
};

export default ZmzButtonGroup;
