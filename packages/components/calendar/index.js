/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzCalendar from './src/zmz-calendar.vue';

ZmzCalendar.install = function(app) {
    app.component(ZmzCalendar.name, ZmzCalendar);
};

export default ZmzCalendar;
