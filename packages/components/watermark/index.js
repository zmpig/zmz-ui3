/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzWatermark from './src/zmz-watermark.vue'

ZmzWatermark.install = function(app) {
    app.component(ZmzWatermark.name, ZmzWatermark)
}

export default ZmzWatermark
