/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzCollapseItem from './src/zmz-collapse-item.vue';

ZmzCollapseItem.install = function(app) {
    app.component(ZmzCollapseItem.name, ZmzCollapseItem);
};

export default ZmzCollapseItem;
