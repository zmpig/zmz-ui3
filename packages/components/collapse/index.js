/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzCollapse from './src/zmz-collapse.vue';

ZmzCollapse.install = function(app) {
    app.component(ZmzCollapse.name, ZmzCollapse);
};

export default ZmzCollapse;
