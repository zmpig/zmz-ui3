/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzDrawer from './src/zmz-drawer.vue';

ZmzDrawer.install = function(app) {
    app.component(ZmzDrawer.name, ZmzDrawer);
};

export default ZmzDrawer;
