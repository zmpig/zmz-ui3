/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzBreadcrumb from './src/zmz-breadcrumb.vue';

ZmzBreadcrumb.install = function(app) {
    app.component(ZmzBreadcrumb.name, ZmzBreadcrumb);
};

export default ZmzBreadcrumb;
