/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzBreadcrumbItem from './src/zmz-breadcrumb-item.vue';

ZmzBreadcrumbItem.install = function(app) {
    app.component(ZmzBreadcrumbItem.name, ZmzBreadcrumbItem);
};

export default ZmzBreadcrumbItem;
