/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzSigned from './src/zmz-signed.vue'

ZmzSigned.install = function(app) {
    app.component(ZmzSigned.name, ZmzSigned)
}

export default ZmzSigned
