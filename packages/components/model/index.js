/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzModel from './src/zmz-model.vue';

ZmzModel.install = function(app) {
    app.component(ZmzModel.name, ZmzModel);
};

export default ZmzModel;
