/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzSticky from './src/zmz-sticky.vue';

ZmzSticky.install = function(app) {
    app.component(ZmzSticky.name, ZmzSticky);
};

export default ZmzSticky;
