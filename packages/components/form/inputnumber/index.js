/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzInputnumber from './src/zmz-inputnumber.vue';

ZmzInputnumber.install = function(app) {
    app.component(ZmzInputnumber.name, ZmzInputnumber);
};

export default ZmzInputnumber;
