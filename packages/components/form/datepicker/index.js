/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzDatePicker from './src/zmz-datepicker.vue';

ZmzDatePicker.install = function(app) {
    app.component(ZmzDatePicker.name, ZmzDatePicker);
};

export default ZmzDatePicker;
