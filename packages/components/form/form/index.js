/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzForm from './src/zmz-form.vue';

ZmzForm.install = function(app) {
    app.component(ZmzForm.name, ZmzForm);
};

export default ZmzForm;
