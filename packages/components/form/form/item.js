/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzFormItem from './src/zmz-form-item.vue';

ZmzFormItem.install = function(app) {
    app.component(ZmzFormItem.name, ZmzFormItem);
};

export default ZmzFormItem;
