/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzTextArea from './src/zmz-textarea.vue'

ZmzTextArea.install = function(app) {
    app.component(ZmzTextArea.name, ZmzTextArea)
}

export default ZmzTextArea
