/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzRadioGroup from './src/zmz-radio-group.vue';

ZmzRadioGroup.install = function(app) {
    app.component(ZmzRadioGroup.name, ZmzRadioGroup);
};

export default ZmzRadioGroup;
