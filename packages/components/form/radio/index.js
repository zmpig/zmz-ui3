/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzRadio from './src/zmz-radio.vue';

ZmzRadio.install = function(app) {
    app.component(ZmzRadio.name, ZmzRadio);
};

export default ZmzRadio;
