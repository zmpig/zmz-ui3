/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzSlider from './src/zmz-slider.vue';

ZmzSlider.install = function(app) {
    app.component(ZmzSlider.name, ZmzSlider);
};

export default ZmzSlider;
