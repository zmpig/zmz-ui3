/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzSwitch from './src/zmz-switch.vue';

ZmzSwitch.install = function(app) {
    app.component(ZmzSwitch.name, ZmzSwitch);
};

export default ZmzSwitch;
