/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzSelect from './src/zmz-select.vue'

ZmzSelect.install = function(app) {
    app.component(ZmzSelect.name, ZmzSelect)
}

export default ZmzSelect
