/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzOptionGroup from './src/zmz-option-group.vue'

ZmzOptionGroup.install = function(app) {
    app.component(ZmzOptionGroup.name, ZmzOptionGroup)
}

export default ZmzOptionGroup
