/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzOption from './src/zmz-option.vue'

ZmzOption.install = function(app) {
    app.component(ZmzOption.name, ZmzOption)
}

export default ZmzOption
