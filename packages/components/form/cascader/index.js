/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzCascader from './src/zmz-cascader.vue'

ZmzCascader.install = function(app) {
    app.component(ZmzCascader.name, ZmzCascader)
}

export default ZmzCascader
