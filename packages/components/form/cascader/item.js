/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzCascaderItem from './src/zmz-cascader-item.vue'

ZmzCascaderItem.install = function(app) {
    app.component(ZmzCascaderItem.name, ZmzCascaderItem)
}

export default ZmzCascaderItem
