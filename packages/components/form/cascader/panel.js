/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzCascaderPanel from './src/zmz-cascader-panel.vue'

ZmzCascaderPanel.install = function(app) {
    app.component(ZmzCascaderPanel.name, ZmzCascaderPanel)
}

export default ZmzCascaderPanel
