/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzRate from './src/zmz-rate.vue';

ZmzRate.install = function(app) {
    app.component(ZmzRate.name, ZmzRate);
};

export default ZmzRate;
