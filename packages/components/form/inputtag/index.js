/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzInputtag from './src/zmz-inputtag.vue';

ZmzInputtag.install = function (app) {
    app.component(ZmzInputtag.name, ZmzInputtag);
};

export default ZmzInputtag;
