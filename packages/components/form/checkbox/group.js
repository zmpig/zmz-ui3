/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzCheckboxGroup from './src/zmz-checkbox-group.vue';

ZmzCheckboxGroup.install = function(app) {
    app.component(ZmzCheckboxGroup.name, ZmzCheckboxGroup);
};

export default ZmzCheckboxGroup;
