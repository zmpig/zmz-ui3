/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzCheckbox from './src/zmz-checkbox.vue';

ZmzCheckbox.install = function(app) {
    app.component(ZmzCheckbox.name, ZmzCheckbox);
};

export default ZmzCheckbox;
