/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzColorpicker from './src/zmz-colorpicker.vue';

ZmzColorpicker.install = function(app) {
    app.component(ZmzColorpicker.name, ZmzColorpicker);
};

export default ZmzColorpicker;
