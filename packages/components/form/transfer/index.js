/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzTransfer from './src/zmz-transfer.vue';

ZmzTransfer.install = function(app) {
    app.component(ZmzTransfer.name, ZmzTransfer);
};

export default ZmzTransfer;
