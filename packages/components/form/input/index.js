/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzInput from './src/zmz-input.vue';

ZmzInput.install = function(app) {
    app.component(ZmzInput.name, ZmzInput);
};

export default ZmzInput;
