/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzUpload from './src/zmz-upload.vue'

ZmzUpload.install = function(app) {
    app.component(ZmzUpload.name, ZmzUpload)
}

export default ZmzUpload
