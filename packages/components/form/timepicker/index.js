/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzTimeicker from './src/zmz-timepicker.vue';

ZmzTimeicker.install = function(app) {
    app.component(ZmzTimeicker.name, ZmzTimeicker);
};

export default ZmzTimeicker;
