/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzBarrage from './src/zmz-barrage.vue';

ZmzBarrage.install = function(app) {
    app.component(ZmzBarrage.name, ZmzBarrage);
};

export default ZmzBarrage;
