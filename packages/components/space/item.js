/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzSpaceItem from './src/zmz-space-item.vue';

ZmzSpaceItem.install = function(app) {
    app.component(ZmzSpaceItem.name, ZmzSpaceItem);
};

export default ZmzSpaceItem;
