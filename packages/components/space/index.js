/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzSpace from './src/zmz-space.js';

ZmzSpace.install = function(app) {
    app.component(ZmzSpace.name, ZmzSpace);
};

export default ZmzSpace;
