/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzAudio from './src/zmz-audio.vue'

ZmzAudio.install = function(app) {
    app.component(ZmzAudio.name, ZmzAudio)
}

export default ZmzAudio
