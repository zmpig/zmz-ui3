/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzPageheader from './src/zmz-pageheader.vue';

ZmzPageheader.install = function(app) {
    app.component(ZmzPageheader.name, ZmzPageheader);
};

export default ZmzPageheader;
