/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzPagefooter from './src/zmz-pagefooter.vue';

ZmzPagefooter.install = function(app) {
    app.component(ZmzPagefooter.name, ZmzPagefooter);
};

export default ZmzPagefooter;
