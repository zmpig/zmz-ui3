/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzPagecontent from './src/zmz-pagecontent.vue';

ZmzPagecontent.install = function(app) {
    app.component(ZmzPagecontent.name, ZmzPagecontent);
};

export default ZmzPagecontent;
