/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzMusic from './src/zmz-music.vue'

ZmzMusic.install = function(app) {
    app.component(ZmzMusic.name, ZmzMusic)
}

export default ZmzMusic
