/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzResult from './src/zmz-result.vue';

ZmzResult.install = function(app) {
    app.component(ZmzResult.name, ZmzResult);
};

export default ZmzResult;
