/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzScrollbar from './src/zmz-scrollbar.js';

ZmzScrollbar.install = function(app) {
    app.component(ZmzScrollbar.name, ZmzScrollbar);
};

export default ZmzScrollbar;
