/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzAnchor from './src/zmz-anchor.vue';

ZmzAnchor.install = function(app) {
    app.component(ZmzAnchor.name, ZmzAnchor);
};

export default ZmzAnchor;
