/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzAnchorItem from './src/zmz-anchor-item.vue';

ZmzAnchorItem.install = function(app) {
    app.component(ZmzAnchorItem.name, ZmzAnchorItem);
};

export default ZmzAnchorItem;
