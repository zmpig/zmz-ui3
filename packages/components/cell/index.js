/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzCell from './src/zmz-cell.vue';

ZmzCell.install = function(app) {
    app.component(ZmzCell.name, ZmzCell);
};

export default ZmzCell;
