/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzIcon from './src/zmz-icon.vue';

ZmzIcon.install = function (app) {
    app.component(ZmzIcon.name, ZmzIcon);
};

export default ZmzIcon;
