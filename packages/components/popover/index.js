/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzPopover from './src/zmz-popover.vue'

ZmzPopover.install = function(app) {
    app.component(ZmzPopover.name, ZmzPopover)
}

export default ZmzPopover
