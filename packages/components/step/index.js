/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzStep from './src/zmz-step.vue'

ZmzStep.install = function(app) {
    app.component(ZmzStep.name, ZmzStep)
}

export default ZmzStep
