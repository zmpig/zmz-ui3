/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzStepItem from './src/zmz-step-item.vue'

ZmzStepItem.install = function(app) {
    app.component(ZmzStepItem.name, ZmzStepItem)
}

export default ZmzStepItem
