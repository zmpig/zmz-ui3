/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzSplit from './src/zmz-split.vue';

ZmzSplit.install = function(app) {
    app.component(ZmzSplit.name, ZmzSplit);
};

export default ZmzSplit;
