/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
import ZmzCarouselItem from './src/zmz-carousel-item.vue';

ZmzCarouselItem.install = function (app) {
  app.component(ZmzCarouselItem.name, ZmzCarouselItem);
};

export default ZmzCarouselItem;
