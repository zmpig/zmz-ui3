/**
 * @author zmz
 * QQ:1028470211
 * Email:1028470211@qq.com
 */
// 组件
export * from './components';
import * as components from './components';
// 指令
import * as directives from './utils/directive.js';
// 全局配置
import zmzGlobal from './utils/global.js';
import * as zmzTools from './utils/index.js';
import { setZindex } from './utils/zIndex.js';


// 样式
import './theme-default/lib/index.css';
// 配置文件
import pack from '../package.json';
// 注册组件
export const install = function install(app, options = {}) {
    const config = {
        size: options.size || '',
        zIndex: options.zIndex || 2000
    }
    Object.keys(components).forEach(function (key) {
        var component = components[key];
        if (component.install) {
            app.use(component, config);
        }
    });
    setZindex(config.zIndex)
    Object.keys(directives).forEach(k => app.directive(k, directives[k]));
    components.ZmzMetaInfo.install(app);
    app.config.globalProperties.$zmzConfig = config;
    app.config.globalProperties.$copy = components.ZmzCopy;
    app.config.globalProperties.$network = components.ZmzNetwork;
    app.config.globalProperties.$loading = components.ZmzLoadingEvent;
    app.config.globalProperties.$loadingbar = components.ZmzLoadingBar;
    app.config.globalProperties.$message = components.ZmzMessage;
    app.config.globalProperties.$toast = components.ZmzToast;
    app.config.globalProperties.$messageBox = components.ZmzMessageBox;
    app.config.globalProperties.$notice = components.ZmzNotice;
    app.config.globalProperties.$zmzGlobal = zmzGlobal(config);
    app.config.globalProperties.$zmzTools = zmzTools;

    return app;
};

export default {
    version: pack.version,
    install: install,
    ...components
};
